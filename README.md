Mapped USA flags a-la [Boetti](https://bl.ocks.org/espinielli/9ea56e041f6847dbe944).



## TODO (?)

* use Mike's [click-to-zoom via transform](http://bl.ocks.org/2206590) in order to experiment with setting svg background on zoom-in.


## References

* U.S. States flags from [Wikipedia](http://en.wikipedia.org/wiki/List_of_U.S._states)

Forked from Mike's [click-to-zoom via transform](http://bl.ocks.org/2206590) in order to experiment with setting svg background on zoom-in.

